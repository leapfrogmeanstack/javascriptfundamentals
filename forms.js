(function(){
	var username = document.forms[0].username;
	var password = document.forms[0].password;
	var inputValue;
	username.addEventListener('focus', function(event){
		inputValue = this.value;
		this.value = "";
	}, false);
	username.addEventListener('blur', function(event){
		this.value = inputValue;
	}, false);
}());