(function(){
	//String
	//Array
	//Math
	//Boolean
	function test(){}
	function test2(){}

	// new Object();
	var car = new Object();
	car.wheels = 4;
	car.color = "black";
	car.model = "Fararri";

	car.go = function(){
		console.log("Going");
	}
	car.increaseSpeed = function(){
		console.log("increasing speed");
	}

	// console.log(car);
	// console.log(car.go);
	// {} Object Literal
	var car = {
		wheels : 4,
		color : null,
		model : null,
		go : function(){
			console.log("Going");
		},
		increaseSpeed : function(){
			console.log("increasing speed");
		}
	};
	// car.test = "test property";
	// console.log(car);
	// console.log(car.wheels);

	// function test(){} new test
	function Car(wheelsCount, color, model){
		this.wheels = wheelsCount;
		this.color = color;
		this.model = model;
		this.go = function(){
			console.log("Going " + this.model);
		};
		this.increaseSpeed = function(){
			console.log("increasing speed");
		};
	}

	var toyota = new Car(4, 'Black', 'Toyota');
	console.log(toyota);
	var ford = new Car(4, 'Red', 'ford');
	console.log(ford);
	console.log(ford.go());
	console.log(toyota.go());

}());