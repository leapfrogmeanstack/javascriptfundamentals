(function(){
	var Person = function(PersonObj, method, args){
		method.apply(PersonObj, args);
		console.log(PersonObj);
		// this.firstName = 'hello';
		// console.log(this instanceof window);
		// this.middleName = null;
		// this.lastName = null;
		// this.age = null;
		// this.education = null;
		// this.getFullName = function(){
		// 	return this.firstName + " " + this.middleName + " " + this.lastName;
		// };
		// console.log(this.firstName);
		// console.log(name, age, occupation);
		console.log(this);
	};

	// var Person = new Person();
	// var TempPerson = {
	// 	firstName: "test",
	// 	asdasdasd : "asdasds"
	// };

	// function myFunction(a, b) {
	// 	var addition = a + b;
	// 	var division = a / b;
	//     return [addition, division];
	// }
	
	// myArray = [10, 2];

	// var myObject = myFunction.apply(myObject, myArray);  // Will also return 20
	
	// console.log(myObject);

	var update = function(name, age, size){
		if(name !== undefined && age !== undefined && size !== undefined){
			this.name = name;
		    this.age = age;
		    this.size = size;
	    	// console.log(this);
		}
		console.log(this);
	};
	
	var person1 = {name: 'Marvin', age: 42, size: '2xM'};

	// update.call(person1, 'Slarty', 200, '1xM')

	var dispatch = function(person, method, args){
		if(args === undefined){
			method.call(person);
		} else {
	    	method.apply(person, args);
		}
	};

	dispatch(person1, update, ['Slarty', 200, '1xM']);

	// function applyFunc(){
	// 	this.firstName = null;
	// 	this.middleName = null;
	// 	this.lastName = null;
	// 	this.age = null;
	// 	this.education = null;
	// 	this.getFullName = function(){
	// 		return this.firstName + " " + this.middleName + " " + this.lastName;
	// 	};
	// }

	// Person(TempPerson, applyFunc, ["Suraj", 27, "Software Engineer"]);
	// console.log(Person.firstName);
}());