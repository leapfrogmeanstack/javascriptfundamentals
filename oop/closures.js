// function sayHello2(name) {
//     var text = 'Hello ' + name; // Local variable
//     var say = function() { console.log(text); console.log(name); }
//     return say;
// }

// var say2 = sayHello2('Bob');

// say2();
// say2();

// function say667() {
//     // Local variable that ends up within closure
//     var num = 666;
//     var say = function() { console.log(num); }
//     num++;
//     return say;
// }
// var sayNumber = say667();
// sayNumber(); // logs 667

// var gLogNumber, gIncreaseNumber, gSetNumber;
// function setupSomeGlobals() {
//     // Local variable that ends up within closure
//     var num = 666;
//     // Store some references to functions as global variables
//     gLogNumber = function() { console.log(num); }
//     gIncreaseNumber = function() { num++; }
//     gSetNumber = function(x) { num = x; }
// }

// setupSomeGlobals();

// gIncreaseNumber();
// gLogNumber(); // 667
// gSetNumber(5);
// gLogNumber(); // 5

// var oldLog = gLogNumber;

// setupSomeGlobals();
// gLogNumber(); // 666

// oldLog() // 5

// function buildList(list) {
//     var result = [];
//     for (var i = 0; i < list.length; i++) {
//         var item = 'item' + i;
//         result.push( function() {console.log(item + ' ' + list[i])} );
//     }
//     return result;
// }

// function testList() {
//     var fnlist = buildList([1,2,3]);
//     // Using j only to help prevent confusion -- could use i.
//     for (var j = 0; j < fnlist.length; j++) {
//         fnlist[j]();
//     }
// }

// testList();

function sayAlice() {
    var say = function() { console.log(alice); }
    // Local variable that ends up within closure
    //say();
    var alice = 'Hello Alice';
    return say;
}
var sayHello = sayAlice();
// sayHello();