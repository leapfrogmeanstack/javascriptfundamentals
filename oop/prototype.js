(function(){
	var Animal = function(){
		this.name = null;
		this.legCount = 4;
		this.color = null;
		this.getName = function(){
			return this.name;
		};
		this.getLegCount = function(){
			return this.legCount;
		};
		this.getColor = function(){
			return this.color;
		};
	};

	new Animal();
	var cat = Object.create(Animal);
	var cat = Object.create(Animal);
		
	var Dog = new Animal();
		Dog.name = "Tiger";
		Dog.legCount = 4;
		Dog.color = "Black";
		Dog.testFunction = function(){
			console.log("test Function");
		};

		Animal.prototype.testProp = "asdasd";
		
	Animal.prototype.getBreed = function(){
		return this.breed;
	};

	// console.log(Dog.constructor.toString());
	console.log(Dog.__proto__.__proto__.__proto__);

	console.log(Dog.__proto__);

	var Bird = new Animal();
		Bird.name = "Parrot";
		Bird.legCount = 2;
		Bird.color = "Light Green";
		Bird.breed = "test breed";

	console.log(Bird.getColor());
	console.log(Bird.getBreed());
}());