var foo = "This is inside window scope or global scope";

// console.log(window.foo);

function TestFunc(){
	var foo = "This is inside local scope";
	console.log(foo);
	console.log(window.foo);
}

// TestFunc();

function convertToString(){
	console.log("convert to string in different way");
}

// convertToString();