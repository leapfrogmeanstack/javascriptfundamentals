function printNestedLoop(){
	for(var j = 1; j <= 5; j++){
	 	for(var k = 1; k <= 5; k++){
	 		console.log(j + " " + k);
	 	}
	}
}


function sayHello(name){
	console.log("Hello " + name + " from JavaScript functions");
}

function add(a, b){
	console.log(a);
	console.log(b);
	var result = a + b;
	return result;
}

//Function Scope
var name = "Leapfrog";

function scopeCheck(){
    name = "Nothing"; 
}
scopeCheck();
console.log(name);

//Nested Functions and Scoping

var car = "Toyota";

function nestedScopeCheck(){
	console.log(car);
	var bike = "Pulsar 150";
	function nestedFunc1(){
		console.log(car);
		console.log(bike);
		function nestedNestedFunc1(){
			console.log(car);
			console.log(bike);
		}
		nestedNestedFunc1();
	}
	function nestedFunc2(){
		console.log(car);
		console.log(bike);
		function nestedNestedFunc1(){
			console.log(car);
			console.log(bike);
		}
		nestedNestedFunc1();
	}
	nestedFunc1();
	nestedNestedFunc1();
	nestedFunc2();
}

nestedScopeCheck();
console.log(bike);


//Anonymous Funtions 

var anonFunc = function(){
	console.log("Hello World from anonymous function");
};

anonFunc();

(function(){
console.log("Hello World from anonymous function");	
})(jQuery, $, _);