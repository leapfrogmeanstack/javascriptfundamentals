(function(){
	var firstBox = document.getElementById("firstbox"),
		style = firstBox.style;

	// style.color = "blue";
	// style.backgroundColor = "yellow";
	// style.padding = "2px";
	// style.width = "200px";

	// console.log(style.color);

	var getStyleProperty = function(element, propertyName){
		if(typeof getComputedStyle !== "undefined"){
			return window.getComputedStyle(element, null).getPropertyValue(propertyName);			
		} else {
			return element.currentStyle[propertyName];		
		}
	};

	// var color = window.getComputedStyle(firstBox, null).getPropertyValue('color');
	
	// var color = firstBox.currentStyle['color'];
	// console.log(firstBox.currentStyle.color);
	// console.log(firstBox.currentStyle.height);

	// var color = getStyleProperty(firstBox, "color");
	// console.log(color);

	var applyClass = function(element, classList){
		if(typeof element.classList !== "undefined"){
			for(var i = 0, len = classList.length; i < len; i++){
				element.classList.add(classList[i]);
			}
		} else {
			// console.log(classList.join(" "));
			firstBox.className = classList.join(" ");
		}
	};
	// applyClass(firstBox, ["test", "test2"]);

	console.log(typeof firstBox.className);
	firstBox.className = "test test2";
	firstBox.className = firstBox.className.replace("test2", "");

	// firstBox.classList.add('test');
	// firstBox.classList.add('test2');

	// firstBox.classList.remove("test2");

}());