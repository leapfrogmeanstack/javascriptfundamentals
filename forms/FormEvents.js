(function(){
	String.prototype.capitalize = function() {
	    return this.charAt(0).toUpperCase() + this.slice(1);
	}

	function generateSpan(content){
		var el = document.createElement('span');
		el.appendChild(document.createTextNode(content));
		return el;
	}

	function generateErrorMsg(el, msg){
		if(el.nextSibling.nodeType !== 1){
			el.parentElement.insertBefore(generateSpan(msg), el.nextSibling);
		}
	}

	function validateFields(fEl){
		var children = [].slice.call(fEl.children);
		children.forEach(function(ele, i, arr){
			if(ele instanceof HTMLInputElement || ele instanceof HTMLTextAreaElement){
				if(ele.type === "text" || ele.type === "password" || ele.type === "textarea"){
					if(ele.value === ""){
				    	generateErrorMsg(ele, ele.name.capitalize() + " is required")
				    }
				} 
				if(ele.type === "radio" || ele.type === "checkbox") {
					if(!ele.checked){
				    	generateErrorMsg(ele, ele.name.capitalize() + " is required")
					}
				}
			}
			if(ele instanceof HTMLSelectElement){
				if(ele.value === ""){
				    generateErrorMsg(ele, ele.name.capitalize() + " is required")
				}
			}
		});
	}

    var signUpForm = document.getElementById("user_signup_form");
 	
 	function submitHandler(evt){
 		evt.stopPropagation();
 		evt.preventDefault();

 		var formvalue = validateFields(signUpForm);
	    
	    var username = signUpForm.username;
	}

 	signUpForm.addEventListener('submit', submitHandler, false);
}());