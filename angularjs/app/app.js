angular.module("LearningApp", ["AssingmentApp"])
.controller("mainCtrl", function($scope){
	$scope.mainMethod = function(){
		console.log("mainMethod from mainCtrl");
	};
})
.directive("helloWorld", function(){
	return {
		restrict: "E",
		template: "Hello World from directive"
	};
});