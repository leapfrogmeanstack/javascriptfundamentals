var todoModule = angular.module("todoModule", ["ngRoute"]);

todoModule.config(["$routeProvider", function($routeProvider){
	$routeProvider.when("/", {
		templateUrl: "partials/todo.html",
		controller: "todoCtrl"
	})
	.when("/notfound", {
		template: "Page not found"
	})
	.otherwise({
		redirectTo: "/notfound",
	});
}]);

todoModule.controller("todoCtrl", function($scope){
	$scope.lists = [];
	$scope.addItemTodo = function(){
		// console.log($scope.todoItem);
		$scope.lists.push($scope.todoItem);
		// console.log($scope.list);
	};
});