(function(){
	var submit = document.getElementById("submit");

	function eventCallBack(event){
		// event.stopPropagation();
		console.log("test");
	}

	function testCallBack(event){
		console.log("hello test");
	}

	function moveOverEvent(event){
		console.log("mouse over");
	}

	function moveOutEvent(event){
		console.log("mouse out");
	}

	function moveOutEnter(event){
		console.log("mouse enter");
	}

	submit.addEventListener('click', eventCallBack, true);
	submit.addEventListener('click', testCallBack, true);

	// submit.attachEvent('onclick', eventCallBack);

	// submit.attachEvent('onclick', testCallBack);
	
	// submit.attachEvent('onmouseover', moveOverEvent);
	
	// submit.attachEvent('onmouseout', moveOutEvent);
	
	// submit.attachEvent('onmouseenter', moveOutEnter);

	console.log(submit.eventListenerList);
	// submit.detachEvent('onclick', testCallBack);
}());