(function(){
	var button = document.getElementById("submit");
	var ulElement = document.getElementById("listItems");

	button.onclick = function(){
		var itemInput = document.getElementById("item");
		var liNode = document.createElement("li");
		liNode.appendChild(document.createTextNode(itemInput.value))
		ulElement.appendChild(liNode);
	};
	
}());