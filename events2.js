(function(){
	var subButton = document.getElementById("submit");
	
	// subButton.onclick = function(){
	// 	console.log("Hello World");	
	// };

	// subButton.onclick = function(){
	// 	console.log("next operation");	
	// };

	var firstCb = function(){
		console.log("first operation");
	};

	var secondCb = function(){
		console.log("second operation");
	};

	subButton.addEventListener("click", firstCb, false);

	subButton.addEventListener('click', secondCb, false);

	subButton.removeEventListener('click', firstCb, false);
}());